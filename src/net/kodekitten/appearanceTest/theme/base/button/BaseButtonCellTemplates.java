package net.kodekitten.appearanceTest.theme.base.button;

import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.sencha.gxt.core.client.XTemplates;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseButtonCellTemplates extends XTemplates {
    @XTemplate("<div class=\"{cls}\" style=\"{styles}\">")
    SafeHtml outer(String cls, SafeStyles styles);

    @XTemplate("<a>{text}</a></div>")
    SafeHtml simpleButton(SafeHtml text);


    @XTemplate("<td valign=middle class=\"{iconWrapClass}\">{imageHtml}</td>")
    SafeHtml icon(String iconWrapClass, SafeHtml imageHtml);

    @XTemplate("<td valign=middle class=\"{iconWrapClass}\">{imageHtml}</td>")
    SafeHtml iconWithStyles(String iconWrapClass, SafeStyles imageStyles, SafeHtml imageHtml);


    @XTemplate("<td valign=middle style=\"{textStyles}\" class=\"{textClass}\" >{text}</td>")
    SafeHtml textWithStyles(String textClass, SafeStyles textStyles, SafeHtml text);

}
