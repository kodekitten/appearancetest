package net.kodekitten.appearanceTest.theme.base.button;

import com.google.gwt.resources.client.CssResource;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseButtonCellStyle extends CssResource {

    String button();

    String hasWidth();

    String iconBottom();

    String iconLeft();

    String iconRight();

    String iconTop();

    String iconWrap();

    String large();

    String mainTable();

    String medium();

    String noIcon();

    String over();

    String small();

    String text();

    String pressed();


}
