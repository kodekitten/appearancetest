package net.kodekitten.appearanceTest.theme.base.button;

import com.google.gwt.resources.client.ClientBundle;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseButtonCellResources extends ClientBundle {
    @Source("BaseButtonCell.css")
    BaseButtonCellStyle style();

}
