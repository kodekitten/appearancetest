package net.kodekitten.appearanceTest.theme.base.button;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safecss.shared.SafeStylesBuilder;
import com.google.gwt.safecss.shared.SafeStylesUtils;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.core.client.dom.XElement;
import com.sencha.gxt.core.client.util.TextMetrics;
import net.kodekitten.appearanceTest.theme.base.frame.BaseFrame;


/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 2:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaseButtonCellAppearance implements ButtonCell.ButtonCellAppearance {
    private BaseButtonCellResources resources;
    private BaseButtonCellStyle style;
    private BaseButtonCellTemplates templates;
    private BaseFrame baseFrame;


    //Constructors

    public BaseButtonCellAppearance() {
        this(GWT.<BaseButtonCellResources>create(BaseButtonCellResources.class));
    }

    public BaseButtonCellAppearance(BaseButtonCellResources resources) {
        this(resources, GWT.<BaseButtonCellTemplates>create(BaseButtonCellTemplates.class));
    }

    public BaseButtonCellAppearance(BaseButtonCellResources resources, BaseButtonCellTemplates templates) {
        this.resources = resources;
        this.templates = templates;
        this.style = resources.style();

        baseFrame = new BaseFrame();
    }


    public XElement getButtonElement(XElement parent) {
        return parent.selectNode("table");
    }


    public XElement getFocusElement(XElement parent) {
        return parent.getFirstChildElement().cast();
    }


    public void onFocus(XElement xElement, boolean b, NativeEvent nativeEvent) {

    }


    public void onOver(XElement xElement, boolean b, NativeEvent nativeEvent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void onPress(XElement xElement, boolean b, NativeEvent nativeEvent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void onToggle(XElement xElement, boolean b) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void render(ButtonCell cell, Cell.Context context, Object value, SafeHtmlBuilder sb) {
        String constantHtml = cell.getHTML();
        boolean hasConstantHtml = constantHtml != null && constantHtml.length() != 0;
        boolean isBoolean = value != null && value instanceof Boolean;
        // is a boolean always a toggle button?
        String text = hasConstantHtml ? cell.getText() : (value != null && !isBoolean)
                ? SafeHtmlUtils.htmlEscape(value.toString()) : "";

        ImageResource icon = cell.getIcon();
        ButtonCell.IconAlign iconAlign = cell.getIconAlign();

        String cls = style.button();

        ButtonCell.ButtonScale scale = cell.getScale();

        switch (scale) {
            case SMALL:
                cls += " " + style.small();
                break;

            case MEDIUM:
                cls += " " + style.medium();
                break;

            case LARGE:
                cls += " " + style.large();
                break;
        }

        SafeStylesBuilder stylesBuilder = new SafeStylesBuilder();

        int width = -1;

        if (cell.getWidth() != -1) {
            int w = cell.getWidth();
            if (w < cell.getMinWidth()) {
                w = cell.getMinWidth();
            }
            stylesBuilder.appendTrustedString("width:" + w + "px;");
            cls += " " + style.hasWidth() + " x-has-width";
            width = w;
        } else {

            if (cell.getMinWidth() != -1) {
                TextMetrics.get().bind(style.text());
                int length = TextMetrics.get().getWidth(text);
                length += 6; // frames

                if (icon != null) {
                    switch (iconAlign) {
                        case LEFT:
                        case RIGHT:
                            length += icon.getWidth();
                            break;
                    }
                }

                if (cell.getMinWidth() > length) {
                    stylesBuilder.appendTrustedString("width:" + cell.getMinWidth() + "px;");
                    cls += " " + style.hasWidth() + " x-has-width";
                    width = cell.getMinWidth();
                }
            }
        }

        final int height = cell.getHeight();
        if (height != -1) {
            stylesBuilder.appendTrustedString("height:" + height + "px;");
        }


        sb.append(templates.outer(cls, stylesBuilder.toSafeStyles()));


        SafeHtmlBuilder inside = new SafeHtmlBuilder();

        if (icon != null) {
            switch (iconAlign) {
                case LEFT:
                    inside.appendHtmlConstant("<tr>");
                    writeIcon(inside, icon, height);
                    if (text != null) {
                        int w = width - (icon != null ? icon.getWidth() : 0) - 4;
                        writeText(inside, text, w, height);
                    }
                    inside.appendHtmlConstant("</tr>");
                    break;
                case RIGHT:
                    inside.appendHtmlConstant("<tr>");
                    if (text != null) {
                        int w = width - (icon != null ? icon.getWidth() : 0) - 4;
                        writeText(inside, text, w, height);
                    }
                    writeIcon(inside, icon, height);
                    inside.appendHtmlConstant("</tr>");
                    break;
                case TOP:
                    inside.appendHtmlConstant("<tr>");
                    writeIcon(inside, icon, height);
                    inside.appendHtmlConstant("</tr>");
                    if (text != null) {
                        inside.appendHtmlConstant("<tr>");
                        writeText(inside, text, width, height);
                        inside.appendHtmlConstant("</tr>");
                    }
                    break;
                case BOTTOM:
                    if (text != null) {
                        inside.appendHtmlConstant("<tr>");
                        writeText(inside, text, width, height);
                        inside.appendHtmlConstant("</tr>");
                    }
                    inside.appendHtmlConstant("<tr>");
                    writeIcon(inside, icon, height);
                    inside.appendHtmlConstant("</tr>");
                    break;
            }


        } else {
            inside.appendHtmlConstant("<tr>");
            if (text != null) {
                writeText(inside, text, width, height);
            }
            inside.appendHtmlConstant("</tr>");
        }


        baseFrame.render(sb, inside.toSafeHtml());
        sb.appendHtmlConstant("</div>");

    }

    private void writeIcon(SafeHtmlBuilder builder, ImageResource icon, int height) {
        SafeHtml iconHtml = AbstractImagePrototype.create(icon).getSafeHtml();
        if (height == -1) {
            builder.append(templates.icon(style.iconWrap(), iconHtml));
        } else {
            int adjustedHeight = height;// - heightOffset;
            SafeStyles heightStyle = SafeStylesUtils.fromTrustedString("height:" + adjustedHeight + "px;");
            builder.append(templates.iconWithStyles(style.iconWrap(), heightStyle, iconHtml));
        }
    }

    private void writeText(SafeHtmlBuilder builder, String text, int width, int height) {
        SafeStylesBuilder sb = new SafeStylesBuilder();
        if (height > 0) {
            int adjustedHeight = height;// - heightOffset;
            sb.append(SafeStylesUtils.fromTrustedString("height:" + adjustedHeight + "px;"));
        }
        if (width > 0) {
            sb.append(SafeStylesUtils.fromTrustedString("width:" + width + "px;"));
        }
        builder.append(templates.textWithStyles(style.text(), sb.toSafeStyles(), SafeHtmlUtils.fromTrustedString(text)));
    }

}
