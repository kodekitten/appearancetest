package net.kodekitten.appearanceTest.theme.base.frame;

import com.google.gwt.resources.client.CssResource;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 8:32 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseFrameStyle extends CssResource {
    String frame();

    String content();

    String contentArea();

    String focus();

    String over();

    String pressed();

}
