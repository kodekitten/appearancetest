package net.kodekitten.appearanceTest.theme.base.frame;

import com.google.gwt.resources.client.ClientBundle;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 8:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseFrameResources extends ClientBundle {
    @Source("BaseFrame.css")
    BaseFrameStyle style();
}
