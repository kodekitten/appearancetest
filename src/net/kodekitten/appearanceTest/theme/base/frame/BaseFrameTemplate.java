package net.kodekitten.appearanceTest.theme.base.frame;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.sencha.gxt.core.client.XTemplates;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 8:23 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseFrameTemplate extends XTemplates {
    @XTemplate(source = "BaseFrame.html")
    SafeHtml render(BaseFrameStyle style, SafeHtml content);

}
