package net.kodekitten.appearanceTest.theme.base.frame;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.sencha.gxt.core.client.resources.StyleInjectorHelper;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaseFrame {
    private BaseFrameTemplate template = GWT.create(BaseFrameTemplate.class);
    private final BaseFrameResources resources;
    private BaseFrameStyle style;

    public BaseFrame() {
        this(GWT.<BaseFrameResources>create(BaseFrameResources.class));
    }

    public BaseFrame(BaseFrameResources resources) {
        this.resources = resources;
        this.style = resources.style();

        StyleInjectorHelper.ensureInjected(this.style, true);
    }

    public void render(SafeHtmlBuilder builder, SafeHtml content) {
        builder.append(template.render(style, content));
    }

}
