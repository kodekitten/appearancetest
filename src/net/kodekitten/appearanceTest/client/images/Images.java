package net.kodekitten.appearanceTest.client.images;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/16/13
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Images extends ClientBundle {
    public Images INSTANCE = GWT.create(Images.class);

    @Source("applicantStatus_black.png")
    ImageResource applicantStatus_default();

    @Source("applicantStatus_white.png")
    ImageResource applicantStatus_alternate();

}
