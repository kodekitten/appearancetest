package net.kodekitten.appearanceTest.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.cell.core.client.TextButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.Viewport;
import net.kodekitten.appearanceTest.client.images.Images;
import net.kodekitten.appearanceTest.theme.base.button.BaseButtonCellAppearance;

/**
 * Created with IntelliJ IDEA.
 * User: cbovee
 * Date: 4/15/13
 * Time: 2:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppearanceTest implements EntryPoint {
    BaseButtonCellAppearance buttonCellAppearance = GWT.create(BaseButtonCellAppearance.class);

    public void onModuleLoad() {
        Viewport viewport = new Viewport();


        TextButtonCell textButtonCell = new TextButtonCell(buttonCellAppearance);
        TextButton textButton = new TextButton(textButtonCell);
        textButton.setText("Non-Standard Button");
        textButton.setIcon(Images.INSTANCE.applicantStatus_default());
        textButton.setIconAlign(ButtonCell.IconAlign.RIGHT);

        TextButton defaultButton = new TextButton();
        defaultButton.setText("DEFAULT");
        defaultButton.setIcon(Images.INSTANCE.applicantStatus_default());
        defaultButton.setIconAlign(ButtonCell.IconAlign.RIGHT);

        HorizontalLayoutContainer container = new HorizontalLayoutContainer();
        container.add(textButton);
        container.add(defaultButton);
        viewport.add(container);

        RootPanel.get().add(viewport.asWidget());
    }
}
